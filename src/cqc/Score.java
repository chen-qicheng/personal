package cqc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class Score {
	@SuppressWarnings({ "unused", "rawtypes" })
	
	public static void main(String[] args) throws IOException{	
        // 导入总分的配置文件
		Properties prop = new Properties();
		prop.load(new FileInputStream("total.properties"));
		Enumeration fileName = prop.propertyNames();
		int all_before = Integer.parseInt(prop.getProperty("before"));
		int all_base = Integer.parseInt(prop.getProperty("base"));
		int all_test = Integer.parseInt(prop.getProperty("test"));
		int all_program = Integer.parseInt(prop.getProperty("program"));
		int all_add = Integer.parseInt(prop.getProperty("add"));
		
		try {
            // 导入大小班课资源
			File file = new File("src/small.html");
			Document doc = Jsoup.parse(file, "UTF-8", "http://example.com/");
			File file1 = new File("src/all.html");
			Document doc1 = Jsoup.parse(file1, "UTF-8", "http://example.com/");

			int self_program=0;
			int self_before=0;
			int self_add=0;	
			int self_test=0;
			int self_base=0;

			

			int rowslength=doc.select("div[class=interaction-row]").size();
			for (int i=0;i<rowslength;i++) {
				int rowssize=doc.select("div[class=interaction-row]").get(i).select("span").size();

				String rows=doc.select("div[class=interaction-row]").get(i).select("span").get(1).text();
				int spansize=doc.select("div[class=interaction-row]").get(i).select("span").size();
				if(rows.indexOf("自测")!=-1) {
					String selfget=doc.select("div[class=interaction-row]").get(i).select("span").get(spansize-1).text();

					self_before+=isNum(selfget);
				}
				if(rows.indexOf("小测")!=-1) {
					String testget=doc.select("div[class=interaction-row]").get(i).select("span").get(spansize-1).text();
					self_test+=isNum(testget);
				}
				if(rows.indexOf("课堂完成")!=-1) {
					String baseget=doc.select("div[class=interaction-row]").get(i).select("span").get(spansize-1).text();
					self_base+=(isNum(baseget));
					
				}
				if(rows.indexOf("编程")!=-1) {
					String proget=doc.select("div[class=interaction-row]").get(i).select("span").get(spansize-1).text();
					self_program+=(isNum(proget));
					
				}
				if(rows.indexOf("附加")!=-1) {
					String addget=doc.select("div[class=interaction-row]").get(i).select("span").get(spansize-1).text();
					self_add+=(isNum(addget));
				}
			}	
			int rowslength1=doc1.select("div[class=interaction-row]").size();//查找html中全部class的个数
			for (int i=0;i<rowslength1;i++) {
				int rowssizea=doc1.select("div[class=interaction-row]").get(i).select("span").size();					
				String rowsa=doc1.select("div[class=interaction-row]").get(i).select("span").get(1).text();
				int spansize=doc1.select("div[class=interaction-row]").get(i).select("span").size();
				if(rowsa.indexOf("自测")!=-1) {
					String selfget=doc1.select("div[class=interaction-row]").get(i).select("span").get(spansize-1).text();
					self_before+=isNum(selfget);
				}
			}
			
			
			double program=self_program/all_program * 100;
			double base=self_base/(float)all_base * 100*0.95;
			double test=self_test/(double)all_test * 100;
			double before=self_before/(double)all_before * 100;
			if(program>95.0)
				program=95.0;
			double add=self_add/all_add* 100;
			if(add>90.0)
				add=90.0;
			double last_score=base*0.3+before*0.25+test*0.2+add*0.05+program*0.1;
			String result = String .format("%.3f",last_score);
			System.out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static int isNum(String str){
		String str1="";
		for(int i=0;i<str.length();i++){
			char chr=str.charAt(i);
			if(chr>=48 && chr<=57)
				str1+=chr;
		}

		int a=Integer.parseInt(str1);
		return a;
	}
	
	
}
